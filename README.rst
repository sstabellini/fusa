Scope
-----

The documents in this repository are documents related to safety
certifications for the Xen Project hypervisor.

The goal is to upstream these documents under xen.git/docs. This
repository is temporary and used to bootstrap an initial consistent set
of documents.


Format
------

All documents are in rst (reStructuredText) format, see:
https://docutils.sourceforge.io/docs/ref/rst/restructuredtext.html.


How to Contribute
-----------------

This project accepts contributions via emails on the Xen FuSa mailing
list as well as GitLab pull requests.


Certificate of Origin
---------------------

By contributing to this project you agree to the Developer Certificate of
Origin (DCO). This document was created by the Linux Kernel community and is a
simple statement that you, as a contributor, have the legal right to make the
contribution. See the https://developercertificate.org file for details.


Mailing List
------------

Please use the Xen FuSa mailing list for questions and patches. You can
subscribe to it here:
https://lists.xenproject.org/mailman/listinfo/fusa-sig.

Use the git format-patch and git send-email commands to generate the
patches and send them to the mailing list.


GitLab
------
This is a rough outline of what a contributor's workflow looks like
using GitLab pull requests:

- Create a topic branch from where you want to base your work (usually master).
- Make commits of logical units.
- Make sure your commit messages are in the proper format (see below).
- Push your changes to a topic branch in your fork of the repository.
- Submit a pull request to the original repository.
- When addressing pull request review comments add new commits to the existing pull request or
  squash them into the existing commits.
- Your contribution is ready to be merged.

Thanks for your contributions!
